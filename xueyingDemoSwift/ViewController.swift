//
//  ViewController.swift
//  xueyingDemoSwift
//
//  Created by wuxueying on 9/22/15.
//  Copyright © 2015 wuxueying. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var confettiView: SAConfettiView!
    var isRainingConfetti = false
    var items = [["1.publicIP--获取公共IP"],
        ["2.SwiftReachability--检查网络是否可用"],
    ["3.KeyboradPopup---键盘弹起","KeyboardPopupViewController"],
    ["4.CalendarViewController---日历","CalendarViewController"],
    ["5.WeChatViewController---分享","WeChatViewController"],
    ["6.BoomViewController---爆炸效果","BoomViewController"],
    ["7.PullRefreshViewController","PullRefreshViewController"],
    ["8.DynamicViewController","DynamicViewController"],
    ["9.\(Device.version()),\(Device.size())"],
    ["10.NotificationPopupViewController","NotificationPopupViewController"],
    ["11.PlaceHolderViewController","PlaceHolderViewController"],
    ["12.InitialViewController","InitialViewController"],
    ["13.DragViewController","DragViewController"],
    ["14.MediaViewController","MediaViewController"],
    ["15.AutoCompleteFieldViewController","AutoCompleteFieldViewController"],
    ["16.LogoGeneratorViewController","LogoGeneratorViewController"],
    ["17.SwitchViewController","SwitchViewController"],
    ["18.AlertViewController","AlertViewController"],
    ["19.YBAlertViewController","YBAlertViewController"],
    ["20.FlipViewController","FlipViewController"],
    ["21.ZEPageControlViewController","ZEPageControlViewController"],
    ["22.BCColorViewController","BCColorViewController"],
    ["23.EasyTransitonViewController","EasyTransitonViewController"],
    ["24.PickerButtonViewController","PickerButtonViewController"],
    ["25.ExpandaleTableViewController","ExpandaleTableViewController"],
    ["26.GoogleSpinnerViewController","GoogleSpinnerViewController"],
    ["27.SquareLoadingViewController","SquareLoadingViewController"],
    ["28.DeepLinkViewController","DeepLinkViewController"],
    ["29.ExpandableconListViewController","ExpandableconListViewController"],
    ["30.CloudTagViewController","CloudTagViewController"],
    ["31.FusumaMediaViewController","FusumaMediaViewController"],
    ["32.MinamoViewController","MinamoViewController"],
    ["33.DropDownViewController","DropDownViewController"],
    ["34.GraphViewController","GraphViewController"],
    ["35.PanGestureViewController","PanGestureViewController"],
    ["36.SignatureViewController","SignatureViewController"],
    ["37.VideoTimeViewController","VideoTimeViewController"],
    ["38.TwitterExampleViewController","TwitterExampleViewController"]]
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reachabilityStatusChanged:", name: reachabilityDidChangeNotification, object: nil)
        addConfettiView()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let ID = "HomeTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(ID, forIndexPath: indexPath) as! HomeTableViewCell
        cell.titleLabel!.text = items[indexPath.row][0]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        [tableView .deselectRowAtIndexPath(indexPath, animated: true)]
        switch indexPath.row {
        case 0:
             showPublicIPAlert()
        case 1:
            break
        default:
            pushToViewController(indexPath)
            break
        }
    }
    
    func pushToViewController(indexPath:NSIndexPath) {
        let identifier = items[indexPath.row][1]
        let storyboard = UIStoryboard(name: identifier, bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier(identifier) as UIViewController!
        if (indexPath.row == 11) {
            self.presentViewController(controller, animated: true, completion: nil)
        } else {
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    // 1.显示公共IP
    func showPublicIPAlert() {
        let ipChecker = APPublicIP();
        ipChecker.checkForCurrentIP({ (ip) -> Void in
            let alert = UIAlertView()
            alert.title = "Alert"
            alert.message = "\(ip)"
            alert.addButtonWithTitle("Understod")
            alert.show()
        }, interval: 0.5.second)
    }
    
    // 2.SwiftReachability--检查网络是否可用
    func reachabilityStatusChanged(notification: NSNotification) {
        if let info = notification.userInfo {
            if let s = info[reachabilityNotificationStatusItem] {
                print(s.description)
            }
        }
    }
    
    @IBAction func addClickEvent(sender: AnyObject) {
        if (isRainingConfetti) {
            confettiView.stopConfetti()
        } else {
            confettiView.startConfetti()
        }
        isRainingConfetti = !isRainingConfetti
    }
    
    func addConfettiView() {
        confettiView = SAConfettiView(frame: self.view.bounds)
        confettiView.colors = [UIColor(red:0.95, green:0.40, blue:0.27, alpha:1.0),
            UIColor(red:1.00, green:0.78, blue:0.36, alpha:1.0),
            UIColor(red:0.48, green:0.78, blue:0.64, alpha:1.0),
            UIColor(red:0.30, green:0.76, blue:0.85, alpha:1.0),
            UIColor(red:0.58, green:0.39, blue:0.55, alpha:1.0)]
        confettiView.intensity = 0.5
        confettiView.type = .Diamond
        confettiView.userInteractionEnabled = false
        view.addSubview(confettiView)
    }

}

