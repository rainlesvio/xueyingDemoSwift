//
//  AppDelegate.swift
//  xueyingDemoSwift
//
//  Created by wuxueying on 9/22/15.
//  Copyright © 2015 wuxueying. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        SSASwiftReachability.sharedManager?.startMonitoring()
        return true
    }

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        
        if MonkeyKing.handleOpenURL(url) {
            return true
        }
        
        return false
    }

    func applicationDidEnterBackground(application: UIApplication) {
        LocalNotificationHelper.sharedInstance.postNotification(key: "mobiwise", title: "Notice", message: "Thanks for use!", seconds: 2)
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
         LocalNotificationHelper.sharedInstance.postNotification(key: "mobiwise", title: "Notice", message: "Thanks for use!", seconds: 2)
    }

}

