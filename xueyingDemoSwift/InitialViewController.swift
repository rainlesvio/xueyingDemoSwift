//
//  InitialViewController.swift
//  ElasticTransitionExample
//
//  Created by Luke Zhao on 2015-12-16.
//  Copyright © 2015 lkzhao. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
  var transition = ElasticTransition()
  let lgr = UIScreenEdgePanGestureRecognizer()
  let rgr = UIScreenEdgePanGestureRecognizer()
  override func viewDidLoad() {
    super.viewDidLoad()
    transition.sticky = true
    transition.showShadow = true
    transition.panThreshold = 0.3
    transition.transformType = .TranslateMid
    
    lgr.addTarget(self, action: "handlePan:")
    lgr.edges = .Left
    view.addGestureRecognizer(lgr)
  }
  
  func handlePan(pan:UIPanGestureRecognizer){
    if pan.state == .Began{
      transition.edge = .Left
      transition.startInteractiveTransition(self, segueIdentifier: "menu", gestureRecognizer: pan)
    }else{
      transition.updateInteractiveTransition(gestureRecognizer: pan)
    }
  }
  
    @IBAction func backClickEvent(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
  @IBAction func codeBtnTouched(sender: AnyObject) {
    transition.edge = .Left
    transition.startingPoint = sender.center
    performSegueWithIdentifier("menu", sender: self)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let vc = segue.destinationViewController
    vc.transitioningDelegate = transition
    vc.modalPresentationStyle = .Custom
    if segue.identifier == "navigation"{
      if let vc = vc as? UINavigationController{
        vc.delegate = transition
      }
    }else{
        
    }
  }
  
}
