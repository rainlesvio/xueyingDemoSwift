//
//  PlaceHolderViewController.swift
//  xueyingDemoSwift
//
//  Created by wuxueying on 1/6/16.
//  Copyright © 2016 wuxueying. All rights reserved.
//

import UIKit

class PlaceHolderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

}
