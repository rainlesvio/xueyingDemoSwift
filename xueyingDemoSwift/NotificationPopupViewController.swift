//
//  NotificationPopupViewController.swift
//  xueyingDemoSwift
//
//  Created by wuxueying on 11/12/15.
//  Copyright © 2015 wuxueying. All rights reserved.
//

import UIKit

class NotificationPopupViewController: UIViewController {

    @IBOutlet var samplePopupView: UIView!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.samplePopupView.hidden = true
    }
    
    func showViewFromTopAction() {
        
        let popup = NotificationAlertView.popupWithView(self.samplePopupView)
        
        popup.show()
        
    }
    
    func hideAnyPopupAction() {
        
        NotificationAlertView.hideAnimated(true)
        
    }
    
    @IBAction func hideFromPopupAction(sender: AnyObject) {
        
        NotificationAlertView.hideAnimated(true)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        showViewFromTopAction()
    }

}
